import './App.css';
import React from 'react';
import MainPage from './modules/mainPage';

function App() {
  return (
    <MainPage/>
  );
}

export default React.memo(App);