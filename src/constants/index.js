const constants = {};

export default Object.defineProperties(constants, {
    LOG_IN               : { value: 'log_in', writable: false },
    SET_USERS            : { value: 'set_users', writable: false },
    IS_LOGGED            : { value: 'is_logged', writable: false },
    SAVE_USER            : { value: 'save_user', writable: false },
    SEARCH_INFO          : { value: 'search_info', writable: false},
    DELETE_CARD          : { value: 'delete_card', writable: false },
    DELETE_USER          : { value: 'delete_user', writable: false },
    LOCK_BUTTON          : { value: 'lock_button', writable: false },
    SAVE_COMMENT         : { value: 'save_comment', writable: false },
    SAVE_PICTURE         : { value: 'save_picture', writable: false },
    SET_COMMENTS         : { value: 'set_comments', writable: false },
    SAVE_ALL_USERS       : { value: 'save_all_users', writable: false },
    NAVBAR_IS_OPEN       : { value: 'navbar_is_open', writable: false },   
    DELETE_COMMENT       : { value: 'delete_comment', writable: false },
    SET_CURRENT_PAGE     : { value: 'set_current_page', writable: false },
    SET_CURRENT_PERSON   : { value: 'set_current_person', writable: false },
    DELETE_CURRENT_PERSON: { value: 'delete_current_person', writable: false },
    CLEAR_COMMENT     : { value: 'clear_comment', writable: false},
})
