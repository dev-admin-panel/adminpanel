import constants from '../../constants';

const initialState = {
    comments:[]
}

export default(state = initialState,action) => {
    switch (action.type) {
        case constants.SAVE_COMMENT:
            return {
                 ...state,
                 comments: [...state.comments,action.payload],
            }
        case constants.DELETE_COMMENT:
            return {
                ...state,
                 comments: state.comments.filter((comments) => comments.id !== +action.payload),
            }
        case constants.SET_COMMENTS:
            return {
                ...state,
                comments: action.payload,
            }
        default:
            return state;
    }
}