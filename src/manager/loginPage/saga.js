import * as api from '../../REST';
import constants from '../../constants'
import { takeEvery, put, call } from 'redux-saga/effects';

export function* watchLoginPage() {
    yield takeEvery(constants.LOG_IN, workerFetchLogIn);
}

function* workerFetchLogIn(action) {
    const result = yield call( api.logIn, action.payload);
    yield put({ type: constants.LOCK_BUTTON });
    yield put({ type: constants.IS_LOGGED, payload: result });
}
