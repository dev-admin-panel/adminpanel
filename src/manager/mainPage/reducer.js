import constants from '../../constants';

const initialState = {
    allUsers: [],
    searchInfo: '',
}

export default (state = initialState, action) => {
    switch(action.type) {
        case constants.SAVE_ALL_USERS:
            return {
                ...state,
                allUsers: action.payload
            }
            
            case constants.SEARCH_INFO:
                return {
                    ...state,
                    searchInfo: action.payload
                }
                
            default:
                 return state;
    }
}