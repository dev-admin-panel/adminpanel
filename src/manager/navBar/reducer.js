import constants from '../../constants';

const initialState = {
    isOpen: true,
    currentPage: '',
}

export default (state = initialState, action) => {
    switch(action.type) {
        case constants.SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.payload
            }
        case constants.NAVBAR_IS_OPEN: 
            return {
                ...state,
                isOpen: state.isOpen ? false : true
            }
           default:
            return state;
    }
}