import constants from '../../constants';

const initialState = {
    currentPerson: {
        id: Date.now(),
        info1: '',
        info2: '',
        gender: '',
        status: 'Mentor',
        lastName: '',
        firstName: '',
        birthDate: '',
        addStatus: '-',
        patronymic: '',
        comments:[]
    }
}

export default (state = initialState, action) => {
    switch(action.type) {
        case constants.SET_CURRENT_PERSON:
            return {
                ...state,
                currentPerson: action.payload
            }
        case constants.DELETE_CURRENT_PERSON:
            return {
                ...state,
                currentPerson: {
                    id: Date.now(),
                    info1: '',
                    info2: '',
                    gender: '',
                    status: 'Mentor',
                    lastName: '',
                    firstName: '',
                    birthDate: '',
                    addStatus: '-',
                    patronymic: '',
                    comments:[]
                }
            }
        case constants.SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.payload
            }
           default:
            return state;
    }
}