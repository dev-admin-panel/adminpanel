import * as api from '../../REST';
import constants from '../../constants'
import { takeEvery, put, call } from 'redux-saga/effects';

export function* watchStaffPage() {
    yield takeEvery(constants.SET_USERS, workerStaffPage);
}

function* workerStaffPage() {
    const result = yield call(api.getAllUsers);
    yield put({ type: constants.SAVE_ALL_USERS, payload: result });
}