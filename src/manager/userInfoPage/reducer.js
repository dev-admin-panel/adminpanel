import constants from '../../constants';

const initialState = {
    saveBtn: false,
    btnDisabled: false,
}

export default (state = initialState, action) => {
    switch(action.type) {
        case constants.LOCK_BUTTON:
            return {
                ...state,
                btnDisabled: true
            }

        case constants.IS_LOGGED:
            return {
                ...state,
                isLogged: action.payload,
                btnDisabled: false
            }
            
        default:
            return state;
    }
}