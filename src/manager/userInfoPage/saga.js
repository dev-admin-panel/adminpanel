import * as api from '../../REST';
import constants from '../../constants'
import { takeEvery, put, call, select  } from 'redux-saga/effects';
import {getComments} from "./selectors";

export function* watchUserInfoPage() {
    yield takeEvery(constants.SAVE_PICTURE, workerUserInfoPageFormData);
    yield takeEvery(constants.SAVE_USER, workerUserInfoPage);
    yield takeEvery(constants.DELETE_USER, workerInfoPageDeleteUser);

}

function* workerUserInfoPage(action) {
    try{
        const comments = yield select(getComments);
        action.payload.comments = comments;
        yield call(api.submitUser, action.payload);
    } catch {
        console.log('osibka')
    }
}

function* workerUserInfoPageFormData(action) {
    try{
        yield call(api.submitFormData, action.payload);
    } catch {
        console.log('osibka')
    }
}

function* workerInfoPageDeleteUser(action) {  
    try{
        yield call(api.deleteUser, action.payload);
    } catch {
        console.log('osibka')
    }
}