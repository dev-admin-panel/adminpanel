import React, { useState, useCallback } from 'react';
import {
    Header,
    Wrapper,
    MainContainer,
    HeaderContainer,
    CommentsWrapper,
    PublicCommentsContainer,
    PrivateCommentsContainer,
    } from "./styledComponent";
import Comment from "./component/Comment.jsx";
import theme from '../../themes/userInfoPageStyles.js'
import { ThemeProvider } from 'styled-components';

const CommentModule = props => {
    const {
        comments,
        saveComment,
        deleteComment,
    } = props

    const [comment, setComment] = useState({
        id: '',
        text: '',
        privateComment: false,
    });

    const onSaveComment = event => {
        event.preventDefault();

        saveComment(comment);
        setComment(preValue => {
            return {
                ...preValue,
                text: '',
            }
        })

    };

    const onChangeText = event => {
        setComment(preValue => {
            return {
                ...preValue,
                text: event.target.value,
                id: new Date().getTime(),
            }
        })
    };

    const isChecked = event => {
        setComment(preValue => {
            return {
                ...preValue,
                privateComment: event.target.checked,
            }
        });
    }

    return(
        <ThemeProvider theme={theme}>
        <Wrapper>
            <Wrapper.MainContainer data-at={'Wrapper__MainContainer'}>
                <Wrapper.Header data-at={'MainContainer_Header'}>
                            <Header.heading data-at={'Header_heading'}
                                            children={'Leave Your Comment'}
                            />
                </Wrapper.Header>
                <MainContainer.HeaderContainer
                    data-at={'MainContainer__HeaderContainer'}
                >
                    <HeaderContainer.textArea data-at={'HeaderContainer_textArea'}
                                              value={comment.text}
                                              onChange={onChangeText}
                    />
                    <HeaderContainer.commentType type={'checkbox'}
                                                 onChange={isChecked}
                    />
                    <HeaderContainer.textPrivate data-at={'HeaderContainer_textPrivate'}    
                                                     children={'Private'}
                    />
                    <HeaderContainer.addCommentBtn onClick={onSaveComment}
                                                   children={'Add'}
                    />
                </MainContainer.HeaderContainer>
                <MainContainer.CommentsWrapper  data-at={'MainContainer_CommentsWrapper'}>
                    <CommentsWrapper.PrivateCommentsContainer
                                            data-at={'CommentsWrapper_PrivateCommentsContainer'}
                    >
                        <PrivateCommentsContainer.PrivateText
                                                data-at={'PrivateCommentsContainer_PrivateText'}
                                                children={'Private'}
                        />
                        { comments.length ?
                            comments.map( (comment,index) =>
                                comment.privateComment ?
                           <Comment id={comment.id}
                                    key={index}
                                    text={comment.text}
                                    deleteComment={deleteComment}
                           /> : null ) :null
                        }
                    </CommentsWrapper.PrivateCommentsContainer>
                    <CommentsWrapper.PublicCommentsContainer 
                                            data-at={'CommentsWrapper_PublicCommentsContainer'}>
                        <PublicCommentsContainer.PublicText
                                                 data-at={'PublicCommentsContainer_PublicText'}
                                                 children={'Public'}
                        /> 
                        { comments.length ?
                            comments.map( (comment,index) =>
                                !comment.privateComment ?
                                    <Comment id={comment.id}
                                             key={index}
                                             text={comment.text}
                                             deleteComment={deleteComment}
                                    /> : null ) :null
                        }
                    </CommentsWrapper.PublicCommentsContainer>
                </MainContainer.CommentsWrapper>
            </Wrapper.MainContainer>
        </Wrapper>
        </ThemeProvider>
    )
}

export default React.memo(CommentModule);