import constants from '../../constants';

export const onSaveComment = payload => ({
    type:constants.SAVE_COMMENT,
    payload,
});
export const onDeleteComment = payload => ({
    type: constants.DELETE_COMMENT,
    payload,
});
