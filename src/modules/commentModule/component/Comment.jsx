import React, {useCallback} from 'react';

import {CommentList} from "../styledComponent.js";

const Comment = props => {
    const {
        id,
        text,
        deleteComment,
    } = props;

    const onDeleteComment = useCallback(event => {
        deleteComment(event.target.id);
    }, [deleteComment]);

    return(
        <CommentList id={id}>
            <CommentList.commentText children={text}/>
            <CommentList.deleteCommentBtn id={id}
                                          onClick={onDeleteComment}
                                          children={'Delete'}
            />
        </CommentList>
    )

}

export default React.memo(Comment);