import Component from './CommentModule.jsx';
import * as selectors from './selectors';
import * as actions from './actions';
import { connect } from "react-redux";

const mapStateToProps = state => ({
    comments: selectors.getComments(state),
});

const mapDispatchToProps = dispatch => ({
    saveComment: payload => dispatch(actions.onSaveComment(payload)),
    deleteComment: payload => dispatch(actions.onDeleteComment(payload)),
});


export default connect(mapStateToProps,mapDispatchToProps)(Component);