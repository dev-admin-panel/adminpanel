import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 1rem;
  align-items: center;
  color: hsl(198, 1%, 29%);
  text-align: center;
  font-size: 130%;
`;

export const MainContainer = styled.div`
  width: 500px;
  background: ${(props) => props.theme.backgroundColor};
  background-size: 25px 25px;
  padding: 1rem;
`;

export const HeaderContainer = styled.form`
  display: flex;
  align-items: center;
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 1rem;
`;

Header.heading = styled.h2`
  padding: 0.2rem 1.2rem;
  border-radius: 20% 5% 20% 5%/5% 20% 25% 20%;
  background-color: ${(props) => props.theme.saveBtnBack};
  font-size: 1.5rem;
  color: ${(props) => props.theme.textColor}; 
`;

HeaderContainer.textArea = styled.textarea`
  width: 300px;
  background-color: ${(props) => props.theme.inputColor};
  padding: 0.7rem;
  border-bottom-right-radius: 15px 3px;
  border-bottom-left-radius: 3px 15px;
  border: solid 3px transparent;
  font-size: 20px;
  color: hsla(260, 2%, 25%, 0.7);
  display: flex;
  resize: none;
  outline: none;
`;

HeaderContainer.addCommentBtn = styled.button`
  width: 70px;
  height: 30px;
  color: white;
  border: none;
  outline: none;
  background: ${(props) => props.theme.saveBtnBack};
  border-radius: 20px;
  cursor: pointer;
  margin-left: 60px;

  :hover {
    background: ${(props) => props.theme.addImageHoverBack};
  }
  :active {
    background: ${(props) => props.theme.addImageActiveBack};
  }
`;

HeaderContainer.commentType = styled.input``;

export const CommentsWrapper = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const PrivateCommentsContainer = styled.ul`
  padding: 0;
  min-height: 100px;
  width: 240px;
  justify-content: space-between;
  border: 2px solid black;
  border-radius: 10px;
`;

export const PrivateComment = styled.li`
  text-align: left;
  position: relative;
  padding: 0.5rem;
  list-style-type: none;
  padding: 0;
`;

PrivateComment.deleteCommentBtn = styled.button`
  width: 70px;
  height: 30px;
  color: white;
  margin: 15px 0 15px 0;
  border: none;
  outline: none;
  background: ${(props) => props.theme.saveBtnBack};
  border-radius: 20px;
  cursor: pointer;

  :hover {
    background: ${(props) => props.theme.addImageHoverBack};
  }
  :active {
    background: ${(props) => props.theme.addImageActiveBack};
  }
`;

PrivateComment.commentText = styled.p``;

export const PublicCommentsContainer = styled.ul`
  padding: 0;
  min-height: 100px;
  width: 240px;
  justify-content: space-between;
  border: 2px solid black;
  border-radius: 10px;
`;

export const CommentList = styled.li`
  display: flex;
  justify-content: space-between;
  align-items: center;  
  list-style-type: none;
  padding-left: 3px;
  margin: 5px;
  border-radius: 5px;
  background: ${(props) => props.theme.inputColor};
  `;

CommentList.deleteCommentBtn = styled.button`
  width: 70px;
  height: 30px;
  color: white;
  border: none;
  outline: none;
  background: ${(props) => props.theme.saveBtnBack};
  border-radius: 20px;
  cursor: pointer;

  :hover {
    background: ${(props) => props.theme.addImageHoverBack};
  }
  :active {
    background: ${(props) => props.theme.addImageActiveBack};
  }
`;

PrivateCommentsContainer.PrivateText = styled.span`
`;

PublicCommentsContainer.PublicText = styled.span`
`;

HeaderContainer.textPrivate = styled.span``;


CommentList.commentText = styled.span`
  
`;

CommentsWrapper.PublicCommentsContainer = PublicCommentsContainer;
CommentsWrapper.PrivateCommentsContainer = PrivateCommentsContainer;
MainContainer.CommentsWrapper = CommentsWrapper;
PublicCommentsContainer.PublicComment = CommentList;
PrivateCommentsContainer.PrivateComment = PrivateComment;
Wrapper.Header = Header;
MainContainer.HeaderContainer = HeaderContainer;
Wrapper.MainContainer = MainContainer;
