import styled from "styled-components";

export const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  border-radius: 10px;
  background-color: ${props => props.theme.backgroundColor};
  display: flex;
  flex-wrap: wrap;
`;
