import React from 'react';
import {
    Wrapper,
} from './styledComponentProfiles';
import PropTypes from 'prop-types';

const Card = props => {
    const {
        id,
        img,
        status,
        fullName,
    } = props;

    return(
        <Wrapper data-at={'card-wrapper'} id = {id}>
            <Wrapper.image data-at= {'wrapper-img'} src={img} alt={'Photo'} id = {id}/>
            <Wrapper.paragraph data-at= {'wrapper-p1'} children={fullName} id = {id}/>
            <Wrapper.paragraph data-at= {'wrapper-p2'} children={status} id = {id}/>
        </Wrapper>
    )
}

Card.propTypes = {
    img: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    fullName: PropTypes.string.isRequired,
}

export default React.memo(Card);
