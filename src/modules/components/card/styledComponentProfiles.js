import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin: 30px;
`;

Wrapper.image = styled.img`
  width: 200px;
  height: 200px;
  border-radius: 50%;
`;

Wrapper.paragraph = styled.p`
  text-align: center;
  font-family: "Roboto", sans-serif;
  font-size: 15px;
`;
