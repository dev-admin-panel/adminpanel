import React from 'react';
import { 
    UserInfoList
} from './styledComponentDropDown.js'

const DropDown = () => {
    return(
        <UserInfoList data-at='dropDown-wrapper'>
            <UserInfoList.select data-at='dropDown-status'>
                <UserInfoList.select.option data-at='dropDown-status_option' children='Mentor'/>
                <UserInfoList.select.option data-at='dropDown-status_option' children='Student'/>
                <UserInfoList.select.option data-at='dropDown-status_option' children='Employee'/>
            </UserInfoList.select>
        </UserInfoList>
    )
}

export default DropDown;