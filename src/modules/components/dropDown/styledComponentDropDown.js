import styled from 'styled-components';

export const UserInfoList = styled.li`
    height: 30px;
    margin-top: 10px;
    display: flex;
    justify-content: flex-end;
`;

UserInfoList.label = styled.p`
    width: 150px;
    margin-right: 10px;
`;

UserInfoList.select = styled.select``
UserInfoList.select.option = styled.option``
