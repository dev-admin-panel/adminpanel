import React, {useEffect} from 'react';
import themes from '../../themes/loginPageStyles.js';
import { Link } from 'react-router-dom';
import {
    Wrapper,
    SearchPanel,
    AddBtnWrapper,
} from './styledComponentHeader';

const Header = props => {

    const {
        url,
        btnText,
        searchInfo
    } = props;
    
    const search = event => {
        searchInfo(event.target.value);
    }

    useEffect(() => {
        return () => {
            searchInfo('');
        }
    }, [searchInfo])

    return(
        <Wrapper data-at={'header_wrapper'}>
            <Wrapper.AddBtnWrapper data-at={'wrapper_add-btn-wrapper'}>
                <Link to={url}>
                    <AddBtnWrapper.addBtn data-at={'add-btn-wrapper_btn'}
                                          children={btnText}
                    />
                </Link>
            </Wrapper.AddBtnWrapper>
            <Wrapper.SearchPanel data-at={'wrapper_search-panel-wrapper'}>
                <SearchPanel.input data-at={'search-panel-wrapper_input'}
                                   placeholder='Search' type='search'
                                   onChange={search}
                />
                <SearchPanel.button src={themes.searchIcon}
                                    data-at={'search-panel-wrapper_btn'}
                />
            </Wrapper.SearchPanel>
        </Wrapper>
    );
}

export default React.memo(Header);