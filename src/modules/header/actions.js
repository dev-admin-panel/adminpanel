import constants from '../../constants'

export const onSearchInfo = payload => ({
    type: constants.SEARCH_INFO,
    payload,
})