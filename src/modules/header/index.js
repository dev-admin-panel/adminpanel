import Component from './Header.jsx';
import * as actions from './actions.js';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
    searchInfo: (payload) => dispatch(actions.onSearchInfo(payload)),
});

export default connect(null, mapDispatchToProps)(Component);