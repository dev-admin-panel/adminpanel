import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  height: 50px;
  border-radius: 10px;
  background: ${(props) => props.theme.headerBackground};
  display: flex;
  justify-content: space-between;
`;

export const AddBtnWrapper = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  margin: 5px 0 0 15px;
`;
AddBtnWrapper.addBtn = styled.button`
  width: 100px;
  height: 35px;
  background-color: ${(props) => props.theme.inputBtnBackground};
  border-radius: 10px;
  border: none;
  cursor: pointer;
  outline: none;
  font-family: "Roboto", sans-serif;
  font-size: 15px;
`;

export const SearchPanel = styled.div`
  height: 40px;
  margin: 5px 0 0 15px;
  display: flex;
  align-items: center;
`;
SearchPanel.input = styled.input`
  width: 300px;
  height: 35px;
  margin: 0 25px 0 0;
  padding: 0 0 0 10px;
  position: relative;
  background-color: ${(props) => props.theme.inputBtnBackground};
  border-radius: 10px;
  border: none;
  outline: none;
  font-family: "Roboto", sans-serif;
  font-size: 15px;
`;

SearchPanel.button = styled.img`
  width: 25px;
  position: absolute;
  margin: 0 0 0 270px;
  cursor: pointer;
`;

Wrapper.SearchPanel = SearchPanel;
Wrapper.AddBtnWrapper = AddBtnWrapper;
