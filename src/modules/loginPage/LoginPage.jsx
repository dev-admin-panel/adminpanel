import React, {useState,useCallback} from 'react';
import { ThemeProvider } from 'styled-components';

import { Inputs,
         Wrapper,
         Container,
         InputWrapper,
         ImageContainer,
        } from './styledComponent.js';
import themes from '../../themes/loginPageStyles.js';

const LoginPage = props => {
    const {
        btnStatus,
        sendUserData,
    } = props;

    const [userData, setUserData] = useState({
        userName:'',
        password:'',
        location: window.location.host
    });

    const handleChange = useCallback( event => {
        const { name, value} = event.target;
        setUserData(preValue => {
            return {
                ...preValue,
                [name]: value
            }
        });
    },[userData]);

    const signIn = useCallback ( () => {
        sendUserData(userData);
    },[userData]);

    return (
        <ThemeProvider theme={themes}>
            <Wrapper data-at={'login_login'}>
            <ImageContainer data-at={'Wrapper_ImageContainer'}>
                <ImageContainer.welcome data-at={'ImageContainer_header'}
                                        children={'Welcome to our world !'}
                />
                <ImageContainer.logo src={themes.loginLogo}
                                     data-at={'ImageContainer_logo'}
                />
            </ImageContainer>
                <Container data-at={'Wrapper_container'}>
                    <Container.header data-at={'Container_header'}
                                      children={'Sign In'} />
                    <Inputs data-at={'Container_inputs'}>
                        <InputWrapper data-at={'input_wrapper'}>
                            <Inputs.label data-at={'label_username'}
                                          children={'USERNAME'}
                            />
                            <Inputs.input name={'userName'}
                                          value={userData.userName}
                                          data-at={'input_username'}
                                          onChange={handleChange}
                                          required={true}
                                          placeholder={'Enter your username'}
                            />
                            <Inputs.label data-at={'label_password'}
                                          children={'PASSWORD'}
                            />
                            <Inputs.input name={'password'}
                                          type={'Password'}
                                          value={userData.password}
                                          data-at={'input_password'}
                                          onChange={handleChange}
                                          required={true}
                                          placeholder={'********'}
                            />
                        </InputWrapper>
                            <Inputs.button data-at={'inputs_button'}
                                           onClick={signIn}
                                           disabled={btnStatus}
                                           children={'Sign In'}
                            />
                    </Inputs>                
                </Container>          
            </Wrapper> 
        </ThemeProvider>        
    )
}

export default React.memo(LoginPage);