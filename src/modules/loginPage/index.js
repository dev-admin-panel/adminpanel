import Component from './LoginPage.jsx';
import * as actions from './actions.js';
import * as selectors from './selectors.js';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    btnStatus: selectors.getBtnStatus(state),
    userStatus: selectors.getUserStatus(state)
});

const mapDispatchToProps = dispatch => ({
    sendUserData: (userData) => dispatch(actions.onSendUserData(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
