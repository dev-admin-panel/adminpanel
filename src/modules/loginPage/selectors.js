export const getBtnStatus = state => state.loginPage.btnDisabled;
export const getUserStatus = state => state.loginPage.isLogged;