import styled from 'styled-components';

export const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display:flex;
`
Wrapper.imageContainer = styled.div`
  width:60%;
  height:100%;
  display:flex;
  flex-direction: column;
  justify-content: space-between;
  background:${props=>props.theme.backgroundColorImage};
`
export const ImageContainer = Wrapper.imageContainer;

ImageContainer.welcome = styled.h2 `
  font-family: 'Roboto', sans-serif;
  font-size:45px;
  text-align:center; 
  padding:0;
  color: ${props=>props.theme.fontColor};
  background:${props=>props.theme.backgroundColorImage};
`
ImageContainer.logo = styled.img`

`
Wrapper.container = styled.div`
  width:40%;
  height:100%;
  background-color: ${props=>props.theme.backgroundColor};
  display:flex;
  justify-content:center;
  flex-direction:column;
  align-items:center;  
`
export const Container = Wrapper.container;

Container.header = styled.h1`
  width:auto;  
  font-family: 'Roboto', sans-serif;   
  padding:0 0 10px 0;
  color: ${props=>props.theme.fontColor};
  border-bottom: solid 1px ${props=>props.theme.backgroundColorImage} ;
  display:flex;
  flex-flow: column wrap;
`
Container.inputs =styled.div`
  height: auto;
  display:flex;
  flex-direction:column;
  justify-content: flex-start;  
`
export const Inputs = Container.inputs;

export const InputWrapper = Container.inputs;

InputWrapper.container = styled.div `
display:flex;
justify-content:flex-start
`

Inputs.label = styled.label `  
  font-family: 'Roboto', sans-serif;
  font-size:13px;  
  color:${props=>props.theme.fontColor};
  background: ${props=>props.theme.backgroundColor}; 
  display:flex;  
  justify-content: flex-start; 
`
Inputs.input=styled.input `
  width:auto;
  font-size:20px;
  font-family: 'Roboto', sans-serif;
  color: ${props=>props.theme.fontColor};
  border:none;
  outline: none;
  background: ${props=>props.theme.backgroundColor};
  margin: 10px 0px 10px 0px;
  border-bottom:2px solid ${props=>props.theme.borderColor};
  display:flex;  
  justify-content: flex-start;
`
Inputs.button = styled.button `
  width: 150px;
  height: 40px;
  background: ${props=>props.theme.backgroundColorImage};
  outline: none;
  border:none;
  font-family: Roboto;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  color: ${props=>props.theme.fontColor};
  border-radius: 40px;
  margin:50px 0 0 0;
  :hover { 
    cursor: pointer;
    background: ${props=>props.theme.hoverColor};
    font-size: 22px;
    font-weight: bold;
   }
`
