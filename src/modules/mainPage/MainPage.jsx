import React, { useEffect } from 'react';
import Navbar from '../navbar';
import StaffPage  from '../staffPage';
import LoginPage from '../loginPage';
import UserInfoPage from '../userInfoPage';
import { CurrentPage, AdminPanel } from './styledComponent.js';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

const MainPage = props => {
    const { userStatus } = props;

    return(
        <>
        {!userStatus ? <LoginPage/>
            :
            <Router>
                <Switch>
                    <AdminPanel>
                        <Navbar/>
                        <CurrentPage data-at={'current-page'}>
                            <Route exact path="/" component={StaffPage}/>
                            <Route path="/students" component={StaffPage}/>
                            <Route path="/mentors" component={StaffPage} />
                            <Route path="/staff" component={StaffPage}/>
                            <Route path="/addPerson" component={UserInfoPage}/>
                            <Route path="/person" component={UserInfoPage}/>
                        </CurrentPage>
                    </AdminPanel>
                </Switch>
            </Router>
        }
       </>
    )
}

export default React.memo(MainPage);