import Component from './MainPage.jsx';
import * as actions from './actions.js';
import * as selectors from './selectors.js';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    userStatus: selectors.getUserStatus(state),
    allUsers: selectors.getAllUsers(state),
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
