export const getUserStatus = state => state.loginPage.isLogged;
export const getAllUsers = state => state.mainPage.allUsers