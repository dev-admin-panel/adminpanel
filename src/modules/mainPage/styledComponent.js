import styled from 'styled-components';

export const AdminPanel = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display:flex;
`
export const CurrentPage = styled.div`
    width: 100%;
    overflow-y: scroll;
`
