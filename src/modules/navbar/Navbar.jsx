import React from 'react';
import { Link }  from 'react-router-dom';
import themes from '../../themes/navbarStyles.js';
import { Wrapper, Header, ButtonContainer, LogOutButton } from './styledComponent.js'; 
import { ThemeProvider } from 'styled-components';

const NavbarModule = props => {
    const {
        logOut,
        setCurrentPage,
        sideBarStatus,
        setSideBarStatus,
    } = props;
    
    const onClick = event => {
        setCurrentPage(event.target.id);
    }
    
    const hideNavbar = () => {
        setSideBarStatus()
    }
    
    const exit = () => {
        logOut()
    }
    
    return (
        <ThemeProvider theme={themes}>
            <Wrapper data-at={'navbar-module_navbar'}
                     sideBarStatus={sideBarStatus}>
                <Wrapper.Header data-at={'Wrapper_header'}>
                    <Header.arrow src={themes.arrowIcon}
                                  onClick={hideNavbar}
                                  sideBarStatus={sideBarStatus}
                    />
                    <Header.logo src={themes.adminIcon}
                                 data-at={'header_logo'}
                                 sideBarStatus={sideBarStatus}
                    />
                    <Header.name children={'Admin'}
                                 data-at={'header_name'}
                    />
                </Wrapper.Header>
                <Link className={'navbar-link'} to={'/'} id='Home'>
                    <Wrapper.ButtonContainer data-at={'wrapper_buttoncontainer'} onClick={onClick} id='Home'>
                        <ButtonContainer.logo src={themes.houseIcon} data-at={'buttonContainer_logo'} id='Home'/>
                        <ButtonContainer.text children={'Home'} data-at={'buttonContainer_text'} id='Home' sideBarStatus={sideBarStatus}/>
                    </Wrapper.ButtonContainer>
                </Link>
                <Link className={'navbar-link'} to={'/mentors'} id='Mentor'>
                    <Wrapper.ButtonContainer data-at={'wrapper_buttonContainer'} onClick={onClick} id='Mentor'>
                        <ButtonContainer.logo src={themes.mentorIcon} data-at={'buttonContainer_logo'} id='Mentor'/>
                        <ButtonContainer.text children={'Mentors'} data-at={'buttonContainer_text'} id='Mentor' sideBarStatus={sideBarStatus}/>
                    </Wrapper.ButtonContainer>
                </Link>
                <Link className={'navbar-link'} to={'/students'} id='Student'>
                    <Wrapper.ButtonContainer data-at={'wrapper_buttonContainer'} onClick={onClick} id='Student'>
                        <ButtonContainer.logo src={themes.studentIcon} data-at={'buttonContainer_logo'} id='Student'/>
                        <ButtonContainer.text children={'Students'} data-at={'buttonContainer_text'} id='Student' sideBarStatus={sideBarStatus}/>
                    </Wrapper.ButtonContainer>
                </Link>
                <Link className={'navbar-link'} to={'/staff'} id='Employee'>
                <Wrapper.ButtonContainer data-at={'wrapper_buttonContainer'} onClick={onClick} id='Employee'>
                    <ButtonContainer.logo src={themes.staffIcon} data-at={'buttonContainer_logo'} id='Employee'/>
                    <ButtonContainer.text children={'Staff'} data-at={'buttonContainer_text'} id='Employee' sideBarStatus={sideBarStatus}/>
                </Wrapper.ButtonContainer>
                </Link>
                <Link className={'navbar-link'} to={'/'}>
                <Wrapper.LogOutButton onClick={exit}>
                    <LogOutButton.logo src={themes.doorIcon} data-at={'logout_logo'}/>
                    <LogOutButton.text children={'Log Out'} data-at={'logOutButton_text'} sideBarStatus={sideBarStatus}/>
                </Wrapper.LogOutButton>
                </Link>  
            </ Wrapper>
        </ThemeProvider>
    )
}

export default React.memo(NavbarModule);