import constants from '../../constants'

export const onSetSideBarStatus = payload => ({
    type: constants.NAVBAR_IS_OPEN,
    payload
})

export const onLogOut = payload => ({
    type: constants.IS_LOGGED,
    payload: false,
})

export const onSetCurrentPage = payload => ({
    type: constants.SET_CURRENT_PAGE,
    payload
})