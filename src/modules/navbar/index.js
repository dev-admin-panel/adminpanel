import Component from './Navbar.jsx';
import * as actions from './actions.js';
import * as selectors from './selectors.js';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    sideBarStatus: selectors.getSideBarStatus(state)
});

const mapDispatchToProps = dispatch => ({
    logOut: () => dispatch(actions.onLogOut()),
    setCurrentPage: (currentPage) => dispatch(actions.onSetCurrentPage(currentPage)),
    setSideBarStatus: () => dispatch(actions.onSetSideBarStatus()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);

