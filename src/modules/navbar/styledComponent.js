import styled from "styled-components";
const backColor = "#2A4473";
const textColor = "#ffffff";
const hoverColor = "#22385E";

export const Wrapper = styled.div`
  width: 230px;
  height: 100vh;
  background: ${backColor};
  transition: width 1s;
  ${(props) => props.sideBarStatus && "width: 90px; height: 100vh; "};
`;

export const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 33%;
`;
Header.logo = styled.img`
  height: 60%;
  width: 60%;
  ${(props) => props.sideBarStatus && "display: none"}
`;
Header.name = styled.span`
  height: 162px;
  tex-align: center;
  color: ${textColor};
`;
Header.arrow = styled.img`
  width: 30px;
  height: 20px;
  margin-left: 84%;
  margin-top: 5%;
  cursor: pointer;
  transition: transform 1.5s;
  ${(props) =>
    props.sideBarStatus &&
    "width: 20px; margin: 10px; cursor: pointer; transform: rotate(180deg);"}
`;

export const ButtonContainer = styled.div`
  display: flex;
  height: 30px;
  align-items: center;
  margin-top: 25px;
  :hover {
    cursor: pointer;
    background: ${hoverColor};
  }
`;
ButtonContainer.logo = styled.img`
  width: 20px;
  height: 20px;
  color: ${textColor};
  margin-left: 28px;
`;
ButtonContainer.text = styled.span`
  color: ${textColor};
  margin-left: 35px;
  text-decoration: none;
  ${(props) => props.sideBarStatus && "display: none"}
`;

export const LogOutButton = styled.div`
  display: flex;
  align-items: flex-end;
  height: 30%;
`;
LogOutButton.logo = styled.img`
  width: 20px;
  height: 20px;
  color: ${textColor};
  margin-left: 35px;
`;
LogOutButton.text = styled.button`
  color: ${textColor};
  border: none;
  cursor: pointer;
  outline: none;
  font-size: 1rem;
  background: ${backColor};
  margin-left: 35px;
  text-decoration: none;
  ${(props) => props.sideBarStatus && "display: none"}
`;

Wrapper.Header = Header;
Wrapper.ButtonContainer = ButtonContainer;
Wrapper.LogOutButton = LogOutButton;
