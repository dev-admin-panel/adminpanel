import React, { useEffect } from 'react';
import { Link }  from 'react-router-dom';
import Card from '../components/card/Card.jsx';
import themes from '../../themes/homePageStyle';
import { ThemeProvider } from 'styled-components';
import { Wrapper } from '../components/Wrapper/Wrapper.js';
import Header from '../header';

const StaffPage = props => {
    const { 
        allStaff,
        searchInfo,
        currentPage,
        setCurrentPerson,
        setAllUsers
    } = props;

    useEffect(()=>{ 
        setAllUsers();        
    }, []);

    let currentCategory = (currentPage === 'Home' || currentPage === '') ? allStaff : allStaff.filter(person => person.status === currentPage);
    currentCategory = currentCategory.filter(person => person.firstName.search(searchInfo) != -1);

    const getUserInfo = event => {
        const currentPerson = currentCategory.find(element => element.id === +event.target.id);

        setCurrentPerson(currentPerson);
    }

    return(
        <ThemeProvider theme={themes}>
            <Header
                url='/addPerson'
                btnText='Add person'
            />
            <Wrapper data-at={'cards-wrapper' } >
                {currentCategory.map(item => {
                    return(
                        <Link to={`/person/${item.id}`} onClick={getUserInfo}>
                            <Card
                                id={item.id}
                                key={item.id}
                                img={item.image}
                                fullName={`${item.firstName} ${item.lastName}`}
                                status={item.status}
                            />
                        </Link>
                    )
                })}
            </Wrapper>
        </ThemeProvider>
    )
}

export default React.memo(StaffPage);