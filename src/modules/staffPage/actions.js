import constants from '../../constants';

export const onSetCurrentPage = payload => ({
    type: constants.SET_CURRENT_PAGE,
    payload
})

export const onSetCurrentPerson = payload => ({
    type: constants.SET_CURRENT_PERSON,
    payload
})

export const onSetAllUsers = () => ({
    type: constants.SET_USERS
})