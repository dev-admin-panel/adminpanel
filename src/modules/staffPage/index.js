import Component from './StaffPage.jsx';
import * as selectors from './selectors.js';
import * as actions from './actions.js';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    allStaff: selectors.getAllStaff(state),
    currentPage: selectors.getCurrentPage(state),
    searchInfo: selectors.getSearchInfo(state),
});

const mapDispatchToProps = dispatch => ({
    setCurrentPage: (currentPage) => dispatch(actions.onSetCurrentPage(currentPage)),
    setCurrentPerson: (currentPerson) => dispatch(actions.onSetCurrentPerson(currentPerson)),
    setAllUsers: () => dispatch(actions.onSetAllUsers())
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
