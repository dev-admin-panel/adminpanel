export const getAllStaff = state => state.mainPage.allUsers;
export const getCurrentPage = state => state.navBar.currentPage;
export const getSearchInfo = state => state.mainPage.searchInfo;
