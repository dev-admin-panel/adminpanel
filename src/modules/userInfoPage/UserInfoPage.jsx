import React, {useState, useCallback, useEffect} from 'react';
import { ThemeProvider } from 'styled-components';
import theme from '../../themes/userInfoPageStyles.js';
import {
    MainWrapper,
    Wrapper,
} from './styledComponent.js'
import UserPhoto from './components/userPhoto/UserPhoto.jsx';
import UserWorkInfo from './components/userWorkInfo/UserWorkInfo.jsx';
import CommentModule from '../commentModule';
import UserPersonalInfo from './components/userPersonalInfo/UserPersonalInfo.jsx';
import ErrorMessage from './components/errorMessage/ErrorMessage.jsx';
import AcceptedMessage from './components/acceptedMessage/AcceptedMessage.jsx';

const UserInfoPage = props => {
    const {
        saveUser,
        deleteUser,
        setComments,
        submitFormData,
        currentPerson,
        deleteCurrentPerson,
    } = props

    useEffect(()=>{ 
        setComments(currentPerson.comments);  
        deleteCurrentPerson();      
    }, []);

    const[currentPageData, setCurrentPageData] = useState({
        curBarName1: '',
        curBarName2: '',
    });

    const[person, setPerson] = useState(currentPerson);
    const[formData, setFormData] = useState({});
    const[file, setFile] = useState({url: '', loaded: true, File: ''});
    const[isError, setisError] = useState(false);
    const[isAccepted, setIsAccepted] = useState(false);

    const setInfo = () => {
        if(person.status == 'Employee'){
            setCurrentPageData(
                {
                    curBarName1: 'Project',
                    curBarName2: 'Department'
                }
            );
        }else if(person.status == 'Mentor'){
            setCurrentPageData(
                {
                    curBarName1: 'Number of Students',
                    curBarName2: 'Department'
                }
            );
        }else if(person.status == 'Student'){
            setCurrentPageData(
                {
                    curBarName1: 'Course',
                    curBarName2: 'Academic perfomance'
                }
            );
        }
    }

    useEffect(() => {
        setInfo();
    },[person.status])

    const handleChange = useCallback(event => {
        const { name, value } = event.target;
        setPerson(preValue => ({
            ...preValue, 
            [name]: value
        }))
        setisError(false);
    }, [person]);

    const handleChangeImg = useCallback(event => {
        const formData = new FormData();
        formData.append('image', event.target.files[0]);
        setFormData(formData);
        getFile(event.target.files[0]);
    }, [formData]);

    const personValues = Object.values(person);

    const savePerson = () => {        
        if(personValues.some(element => element === '')){
            setisError(true);
            setIsAccepted(false);
        }else{
            submitFormData(formData);
            saveUser(person);
            setIsAccepted(true);
            setisError(false);
        }
    }
    
    const deletePerson = () => {
        const id  = { id: person.id };
        deleteUser(id);
    }

    const getFile = inputData => {
        const imgURL = URL ? URL.createObjectURL(inputData): '';
        const files = inputData;

        setFile({url: imgURL, loaded: true, File: files});
    };

    return (
        <ThemeProvider theme={theme}>
            <MainWrapper>
                <Wrapper data-at={'wrapper_container'}>
                {isError && <ErrorMessage/>}
                {isAccepted && <AcceptedMessage />}
                    <Wrapper.pageTitle data-at={'container_page-title'}
                                       children={'User info'}
                    />               
                    <Wrapper.line/>                    
                    <Wrapper.UserInfoContainer data-at={'container_user-info-container'}>
                        <UserPersonalInfo
                            gender={person.gender}
                            lastName={person.lastName}
                            firstName={person.firstName}
                            birthDate={person.birthDate}
                            patronymic={person.patronymic}
                            handleChange={handleChange}
                        />
                        <UserWorkInfo
                            curBarName1={currentPageData.curBarName1}
                            curBarName2={currentPageData.curBarName2}
                            info1={person.info1}
                            info2={person.info2}
                            status={person.status}
                            addStatus={person.addStatus}
                            handleChange={handleChange}
                        />
                    </Wrapper.UserInfoContainer>
                    <Wrapper.line/>
                    <UserPhoto
                        handleChangeImg={handleChangeImg}
                        src={file.url}
                    />
                </Wrapper>
                <CommentModule/>                
                <MainWrapper.saveBtn children='Save' onClick={savePerson}/>
                <MainWrapper.deleteBtn children='Delete' onClick={deletePerson}/>
            </MainWrapper>
        </ThemeProvider>
    );
}

export default React.memo(UserInfoPage);
