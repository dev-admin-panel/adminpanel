import constants from '../../constants';

export const onSaveUser = payload => ({
    type: constants.SAVE_USER,
    payload
})

export const setComments = payload => ({
    type: constants.SET_COMMENTS,
    payload
})

export const onSubmitFormData = payload => ({
    type: constants.SAVE_PICTURE,
    payload
})

export const onDeleteUser = payload => ({
    type: constants.DELETE_USER,
    payload
})

export const onSetAllUsers = () => ({
    type: constants.SET_USERS
})

export const onDeleteCurrentPerson = () => ({
    type: constants.DELETE_CURRENT_PERSON
})
