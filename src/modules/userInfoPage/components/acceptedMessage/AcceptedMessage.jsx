import React from 'react';
import {   
    Wrapper,
} from './styleComponent.js'


const AcceptedMessage = () => {
    return(
               
        <Wrapper>
            <Wrapper.text children={'Accepted !'}/>
        </Wrapper>       
            
    )

}

export default React.memo(AcceptedMessage);