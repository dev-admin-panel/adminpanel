import styled from "styled-components";

export const Wrapper = styled.div`
  height: 40px;
  width: 220px;
  background: #DFF2BF;
  display: flex;
  border-radius: 12px;
  align-items: center;
  justify-content: center;
  border: 1px dashed blue;
`;

Wrapper.text = styled.p`
  color: #4F8A10;
`;
