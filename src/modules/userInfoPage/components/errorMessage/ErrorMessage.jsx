import React from 'react';
import {   
    Wrapper,
} from './styleComponent.js'


const ErrorMessage = () => {
    return(
               
        <Wrapper>
            <Wrapper.text children={'Please fill all inputs'}/>
        </Wrapper>       
            
    )

}

export default React.memo(ErrorMessage);