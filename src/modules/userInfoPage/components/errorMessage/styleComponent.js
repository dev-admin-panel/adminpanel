import styled from "styled-components";

export const Wrapper = styled.div`
  height: 40px;
  width: 220px;
  background: #FFD2D2;
  display: flex;
  border-radius: 12px;
  align-items: center;
  justify-content: center;
  border: 1px dashed blue;
`;

Wrapper.text = styled.p`
  color: #D8000C;
`;
