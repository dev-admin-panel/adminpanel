import React from 'react';
import {
    UserInfoList,
    UserInfoContainer,
} from '../../styledComponent.js'

const UserPersonalInfo = props => {
    const {
        gender,
        lastName,
        firstName,
        birthDate,
        patronymic,
        handleChange,
    } = props;

    return(
        <>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label data-at={'user-info-list_label'}
                                    children='First name'
                />
                <UserInfoList.input name='firstName'
                                    value={firstName}
                                    onChange={handleChange}
                />
            </UserInfoContainer.UserInfoList>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label data-at={'user-info-list_label'}
                                    children='Last name'
                />
                <UserInfoList.input name='lastName'
                                    value={lastName}
                                    onChange={handleChange}
                />
            </UserInfoContainer.UserInfoList>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label data-at={'user-info-list_label'}
                                    children='Patronymic'
                />
                <UserInfoList.input name='patronymic'
                                    value={patronymic}
                                    onChange={handleChange}
                />
            </UserInfoContainer.UserInfoList>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label data-at={'user-info-list_label'}
                                    children='Date of birth'
                />
                <UserInfoList.input type='date'
                                    name='birthDate'
                                    value={birthDate}
                                    onChange={handleChange}
                />
            </UserInfoContainer.UserInfoList>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label data-at={'user-info-list_label'}
                                    children='Gender'
                />
                <UserInfoList.select data-at={'user-info-list_selecet'}
                                        onChange={handleChange}
                                        name='gender'
                                        value={gender}>
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='Male'
                                                value='male'
                    />
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='Female'
                                                value='female'
                    />
                </UserInfoList.select>
            </UserInfoContainer.UserInfoList>
        </>
    )
}

export default React.memo(UserPersonalInfo);