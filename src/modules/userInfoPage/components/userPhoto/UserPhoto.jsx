import React from 'react';
import {
    Wrapper,
    UserPhotoContainer,
} from '../../styledComponent.js'

const UserPhoto = props => {
    const {
        handleChangeImg,
        src
    } = props;
    return(
        <Wrapper.UserPhotoContainer data-at={'container_user-photo-container'}>
            <UserPhotoContainer.image  data-at= {'user-img'} src={src} />
            <UserPhotoContainer.userPhotoLabel data-at={'user-photo-container_label'}
            >Add Photo
            <UserPhotoContainer.UserPhoto   data-at={'user-photo-container_user-photo'}
                                            type={'file'}
                                            // value={person.image}
                                            accept={'image'}
                                            placeholder={'Add Image'}
                                            onChange={handleChangeImg}
                                            />
            </UserPhotoContainer.userPhotoLabel>
        </Wrapper.UserPhotoContainer>
    )
}

export default React.memo(UserPhoto);