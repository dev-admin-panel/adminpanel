import React from 'react';
import {
    UserInfoList,
    UserInfoContainer,
} from '../../styledComponent.js'

const UserWorkInfo = props => {

    const {
        info1,
        info2,
        status,
        addStatus,
        curBarName1,
        curBarName2,
        handleChange,
    } = props;

    return(
        <>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label  data-at={'user-info-list_label'}
                                        children='Status'
                />
                <UserInfoList.select data-at={'user-info-list_selecet'}
                                        onChange={handleChange}
                                        name='status'
                                        value={status}>
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='Mentor'
                                                value='Mentor'
                    />
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='Student'
                                                value='Student'
                    />
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='Employee'
                                                value='Employee'
                    />
                </UserInfoList.select>
            </UserInfoContainer.UserInfoList>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label  data-at={'user-info-list_label'}
                                        children='Add status'
                />
                <UserInfoList.select data-at={'user-info-list_selecet'}
                                        onChange={handleChange}
                                        name='addStatus'
                                        value={addStatus}>
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='-'
                                                value='-'
                    />                        
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='Mentor'
                                                value='Mentor'
                    />
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='Student'
                                                value='Student'
                    />
                    <UserInfoList.select.option data-at={'selecet_select-option'}
                                                children='Employee'
                                                value='Employee'
                    />
                </UserInfoList.select>
            </UserInfoContainer.UserInfoList>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label data-at={'user-info-list_label'} children={curBarName1}/>
                <UserInfoList.input name='info1'
                                    value={info1}
                                    onChange={handleChange}
                />
            </UserInfoContainer.UserInfoList>
            <UserInfoContainer.UserInfoList data-at={'user-info-container_user-info-list'}>
                <UserInfoList.label data-at={'user-info-list_label'} children={curBarName2}/>
                <UserInfoList.input name='info2'
                                    value={info2}
                                    onChange={handleChange}
                />
            </UserInfoContainer.UserInfoList>
        </>
    )
}

export default React.memo(UserWorkInfo);