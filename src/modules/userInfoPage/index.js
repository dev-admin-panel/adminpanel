import Component from './UserInfoPage.jsx';
import * as actions from './actions.js';
import * as selectors from './selectors.js';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
    currentPage: selectors.getCurrentPage(state),
    currentPerson: selectors.getCurrentPerson(state),
});

const mapDispatchToProps = dispatch => ({
    saveUser: payload => dispatch(actions.onSaveUser(payload)),
    setComments: payload => dispatch(actions.setComments(payload)),
    submitFormData: userData => dispatch(actions.onSubmitFormData(userData)),
    deleteUser: userData => dispatch(actions.onDeleteUser(userData)),
    setAllUsers: () => dispatch(actions.onSetAllUsers()),
    deleteCurrentPerson: () => dispatch(actions.onDeleteCurrentPerson()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
