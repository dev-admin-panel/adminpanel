export const getCurrentPage = state => state.navBar.currentPage;
export const getCurrentPerson = state => state.staffPage.currentPerson;
