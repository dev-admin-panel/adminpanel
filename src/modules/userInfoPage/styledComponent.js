import styled from 'styled-components';

export const MainWrapper = styled.div`
    display: flex;
    margin-top: 20px;
`
MainWrapper.saveBtn=styled.button`
    width: 70px;
    height: 30px;
    color: white;
    border: none;
    outline: none;
    background: ${props => props.theme.saveBtnBack};
    border-radius: 20px;
    margin: 5px;
    cursor: pointer;

    :hover {
        background: ${props => props.theme.addImageHoverBack};
    }
    :active {
        background: ${props => props.theme.addImageActiveBack};
    }
`;

MainWrapper.deleteBtn=styled.button`
    width: 70px;
    height: 30px;
    color: white;
    border: none;
    outline: none;
    margin: 5px;
    background: ${props => props.theme.saveBtnBack};
    border-radius: 20px;
    cursor: pointer;

    :hover {
    background: ${props => props.theme.addImageHoverBack};
    }
    :active {
    background: ${props => props.theme.addImageActiveBack};
    }
`;

export const Wrapper = styled.div`
    width: 500px;
    margin-left: 20px;
`;
Wrapper.line = styled.hr``;
Wrapper.pageTitle = styled.h2``;

export const UserPhotoContainer = styled.div`
    height: 250px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

UserPhotoContainer.userPhotoLabel = styled.label`
  width: 100px;
  height: 30px;
  color: white;
  border: none;
  outline: none;
  margin-top: 10px;
  border-radius: 20px;
  cursor: pointer;
  display: flex;
  align-items: center;  
  justify-content: center;
  background: ${props => props.theme.saveBtnBack};
  :hover {
      background: ${props => props.theme.addImageHoverBack};
  }
  :active {
      background: ${props => props.theme.addImageActiveBack};
  }
`;

export const UserPhoto = styled.input`
    display: none;
`;

UserPhotoContainer.image = styled.img`
    width: 150px;
    height: 150px;
    outline: none;  
    border: none;
    border-radius: 100px;
`
UserPhoto.imgTitle = styled.span``;

export const UserInfoContainer = styled.ul`
    list-style: none;
`;

export const UserInfoList = styled.li`
    height: 30px;
    margin-top: 10px;
    display: flex;
    align-items: center;
`;
UserInfoList.label = styled.p`
    width: 150px;
    margin-right: 10px;
`;
UserInfoList.input = styled.input`
    width: 250px;
    height: 20px;
    background: ${props => props.theme.inputColor};
    border: none;
    border-radius: 5px;
    outline: none;
`;
UserInfoList.select = styled.select``;
UserInfoList.select.option = styled.option``;

MainWrapper.Wrapper = Wrapper;
Wrapper.UserInfoContainer = UserInfoContainer;
Wrapper.UserPhotoContainer = UserPhotoContainer;
UserPhotoContainer.UserPhoto = UserPhoto;
UserInfoContainer.UserInfoList = UserInfoList;
