import { combineReducers } from 'redux';
import loginPage from '../../manager/loginPage/reducer.js';
import staffPage from '../../manager/staffPage/reducer.js';
import mainPage from '../../manager/mainPage/reducer.js';
import commentModule from '../../manager/commentModule/reducer.js';
import navBar from '../../manager/navBar/reducer.js';

export default combineReducers({
    navBar,
    mainPage,
    staffPage,
    loginPage,
    commentModule,
});
