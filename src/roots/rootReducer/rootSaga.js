import { all, call } from 'redux-saga/effects'

import { watchLoginPage } from '../../manager/loginPage/saga.js';
import { watchUserInfoPage } from '../../manager/userInfoPage/saga.js';
import { watchStaffPage } from '../../manager/staffPage/saga.js';

const sagaList = [
    watchLoginPage,
    watchUserInfoPage,
    watchStaffPage
];

export function* watchRootSaga() {
    yield all(sagaList.map(saga => call(saga)));
}
