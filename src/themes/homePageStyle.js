const homePageStyle = {
  fontColor: '#FFFFFF',
  backgroundColor: '#EBF0F7',
  headerBackground: '#FFFFFF',
  inputBtnBackground: '#ededed',
  searchIcon: require('../../public/assets/search-icon.svg').default,
};

export default homePageStyle;
