const loginPageStyles = {
    fontColor: '#FFFFFF',
    loginLogo: require('../../public/assets/rocket.svg').default,
    hoverColor: '#0368fa',
    backgroundColor: '#324359',
    backgroundColorImage: '#61D4C3',
    borderColor: '#495D72',
}

export default loginPageStyles;
