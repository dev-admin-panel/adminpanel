const navbarStyles = {
    doorIcon: require('../../public/assets/door-closed.svg').default,
    houseIcon: require('../../public/assets/house-icon.svg').default,
    staffIcon: require('../../public/assets/staff-icon.svg').default,
    adminIcon: require('../../public/assets/admin.png').default,
    arrowIcon: require('../../public/assets/arrow-icon.svg').default,
    mentorIcon: require('../../public/assets/mentor-icon.svg').default,
    studentIcon: require('../../public/assets/student-icon.svg').default,
}

export default navbarStyles;
