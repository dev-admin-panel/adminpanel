const userInfoPageStyle = {
  textColor: '#FFFFFF',
  inputColor: '#DDE1E8',
  saveBtnBack: '#2A4473',
  addImageHoverBack: '#0000CD',
  addImageActiveBack: '#191970',
};
  
  export default userInfoPageStyle;
  